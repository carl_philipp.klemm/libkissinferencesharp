using System;

namespace Kiss
{

	/// <summary>
	/// This exception not directly used by libkissinferencesharp, but all exeptions used inherit from this type.
	/// </summary>
	public class KissException : Exception
	{
		public KissException(){}
		public KissException(string message): base(message){}
		public KissException(string message, Exception inner): base(message, inner){}
	}

	/// <summary>
	/// This exception is thrown when an error occures during inference, usually due to running out of memory.
	/// </summary>
	public class InferenceException : KissException
	{
		public InferenceException(){}
		public InferenceException(string message): base(message){}
		public InferenceException(string message, Exception inner): base(message, inner){}
	}

	/// <summary>
	/// This exception is thrown a user of this libary attempts to instantate an object in an enviroment that is unsupported.
	/// </summary>
	public class EnviromentException : KissException
	{
		public EnviromentException(){}
		public EnviromentException(string message): base(message){}
		public EnviromentException(string message, Exception inner): base(message, inner){}
	}

}


