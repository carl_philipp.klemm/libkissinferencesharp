using System;
using System.Runtime.InteropServices;

namespace Kiss
{

public class Translators
{
	public static string EisToCdc(string input)
	{
		Utils.CheckEnvThrow();
		var ptr = Capi.kiss_eis_to_cdc(input);
		var managedString = Capi.UTF8StringFromPointer(ptr);
		Capi.free(ptr);
		return managedString;
	}

	public static string EisToRelaxIs(string input)
	{
		Utils.CheckEnvThrow();
		var ptr = Capi.kiss_eis_to_relaxis(input);
		var managedString = Capi.UTF8StringFromPointer(ptr);
		Capi.free(ptr);
		return managedString;
	}
}

}
