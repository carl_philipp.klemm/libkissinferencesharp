using System.Runtime.InteropServices;

namespace Kiss
{

/// <summary>
/// Struct <c>VersionFixed</c> reprisents the version of kissinference
/// </summary>
[StructLayout (LayoutKind.Sequential)]
public struct VersionFixed
{
	public int Major;
	public int Minor;
	public int Patch;

	/// <summary>
	/// This Method returns the version of the currently loaded kissinference instance.
	/// </summary>
	public static VersionFixed GetVersion()
	{
		Utils.CheckEnvThrow();
		return GetVersionNoCheck();
	}

	/// <summary>
	/// This Method returns the version of the currently loaded kissinference instance.
	/// </summary>
	[DllImport("kissinference", CharSet = CharSet.Ansi, SetLastError = true, EntryPoint = "kiss_get_version", CallingConvention=CallingConvention.StdCall)]
	public static extern VersionFixed GetVersionNoCheck();

	public override string ToString()
	{
		return Major.ToString() + "." + Minor.ToString() + "." + Patch.ToString();
	}
}

}
