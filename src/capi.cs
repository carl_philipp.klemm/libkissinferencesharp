using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;

namespace Kiss
{

/// <summary>
/// This struct is a raw repisenstion of the C struct used in libkissiniference. Ususally users of these bindings will not need to directly use this.
/// </summary>
[StructLayout (LayoutKind.Sequential)]
public struct CNetwork
{
	private IntPtr _private;
	public UIntPtr inputSize;
	public UIntPtr outputSize;
	public IntPtr outputMask;
	public IntPtr purpose;
	public IntPtr inputLabel;
	public IntPtr outputLabels;
	public byte complexInput;
	public byte ready;
	public byte softmax;
	private IntPtr _resultCb;
}

internal class Capi
{
	public delegate void resultDlg(IntPtr result, ref CNetwork network, IntPtr data);

	[DllImport("kissinference", SetLastError = true, EntryPoint = "kiss_free", CallingConvention=CallingConvention.StdCall)]
	public static extern void free(IntPtr ptr);

	[DllImport("kissinference", SetLastError = true, CallingConvention=CallingConvention.StdCall)]
	public static extern IntPtr kiss_create_range(float start, float end, UIntPtr length, byte log);

	[DllImport("kissinference", SetLastError = true, CallingConvention=CallingConvention.StdCall)]
	public static extern void kiss_resample_spectra(float[] in_re, float[] in_im, UIntPtr input_length, ref IntPtr ret_ptr_real, ref IntPtr ret_ptr_imag, UIntPtr output_length);

	[DllImport("kissinference", SetLastError = true, CallingConvention=CallingConvention.StdCall)]
	public static extern void kiss_normalize_spectra(float[] in_re, float[] in_im, UIntPtr input_length);

	[DllImport("kissinference", SetLastError = true, CallingConvention=CallingConvention.StdCall)]
	public static extern IntPtr kiss_absgrad(float[] in_re, float[] in_im, float[] omegas, UIntPtr input_length, UIntPtr index);

	[DllImport("kissinference", SetLastError = true, CallingConvention=CallingConvention.StdCall)]
	public static extern float kiss_grad(float[] data, float[] omegas, UIntPtr input_length, UIntPtr index);

	[DllImport("kissinference", SetLastError = true, CallingConvention=CallingConvention.StdCall)]
	public static extern float kiss_median(float[] data, UIntPtr input_length);

	[DllImport("kissinference", SetLastError = true, CallingConvention=CallingConvention.StdCall)]
	public static extern byte kiss_reduce_spectra(float[] in_re, float[] in_im, float[] omegas, UIntPtr input_length, float thresh_factor,
												  byte use_second_deriv, ref IntPtr out_re, ref IntPtr out_im, ref UIntPtr output_length);

	[DllImport("kissinference", SetLastError = true, CallingConvention=CallingConvention.StdCall)]
	public static extern byte kiss_filter_spectra(float[] in_re, float[] in_im, float[] omegas, UIntPtr input_length,
												  ref IntPtr out_re, ref IntPtr out_im, UIntPtr output_length);

	[DllImport("kissinference", SetLastError = true, CallingConvention=CallingConvention.StdCall)]
	public static extern byte kiss_load_network_prealloc(ref CNetwork net, byte[] path, resultDlg callback, byte verbose);

	[DllImport("kissinference", SetLastError = true, CallingConvention=CallingConvention.StdCall)]
	public static extern void kiss_free_network_prealloc(ref CNetwork net);

	[DllImport("kissinference", SetLastError = true, CallingConvention=CallingConvention.StdCall)]
	public static extern byte kiss_async_run_inference(ref CNetwork net, [Out] float[] inputs, IntPtr data);

	[DllImport("kissinference", SetLastError = true, CallingConvention=CallingConvention.StdCall)]
	public static extern byte kiss_async_run_inference_complex(ref CNetwork net, [Out] float[] real, [Out] float[] imag, IntPtr data);

	[DllImport("kissinference", CharSet = CharSet.Ansi, SetLastError = true, CallingConvention=CallingConvention.StdCall)]
	public static extern byte kiss_set_output_mask(ref CNetwork net, [Out] byte[] output_mask);

	[DllImport("kissinference", SetLastError = true, CallingConvention=CallingConvention.StdCall)]
	public static extern IntPtr kiss_get_strerror(ref CNetwork net);

	[DllImport("kissinference", SetLastError = true, CallingConvention=CallingConvention.StdCall)]
	public static extern byte kiss_float_eq(float a, float b, uint ulp);

	[DllImport("kissinference", CharSet = CharSet.Ansi, SetLastError = true, CallingConvention=CallingConvention.StdCall)]
	public static extern IntPtr kiss_eis_to_cdc(string input);

	[DllImport("kissinference", CharSet = CharSet.Ansi, SetLastError = true, CallingConvention=CallingConvention.StdCall)]
	public static extern IntPtr kiss_eis_to_relaxis(string input);

	public static string[] IntPtrToUtf8Array(IntPtr array)
	{
		var pointers = new List<IntPtr>();
		var size = Marshal.SizeOf<IntPtr>();
		var idx = 0;
		while (true)
		{
			var ptr = Marshal.ReadIntPtr(array, idx * size);
			if (ptr == IntPtr.Zero)
				break;
			pointers.Add(ptr);
			idx++;
		}

		var res = new List<string>();
		foreach (var p in pointers)
			res.Add(UTF8StringFromPointer(p));

		return res.ToArray();
	}

	public static string UTF8StringFromPointer(IntPtr ptr)
	{
		var bytes = new List<byte>(128);
		var idx = 0;
		while (true)
		{
			var b = Marshal.ReadByte(ptr, idx);
			if (b == 0)
				break;

			bytes.Add(b);
			idx++;
		}

		return System.Text.Encoding.UTF8.GetString(bytes.ToArray());
	}

	public static bool[] IntPtrToBoolArray(IntPtr ptr, int size)
	{
		var output = new bool[size];
		for(int i = 0; i < size; ++i)
			output[i] = Convert.ToBoolean(Marshal.ReadByte(ptr, i));
		return output;
	}

	public static byte[] StringToPlatformBytes(String input)
	{
		if(RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
			return System.Text.Encoding.Unicode.GetBytes(input);
		else
			return System.Text.Encoding.UTF8.GetBytes(input);
	}
}

}
