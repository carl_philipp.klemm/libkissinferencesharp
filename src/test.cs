using System;
using System.Threading;
using System.Linq;
using Kiss;

public class Program
{
	private static void PrintSpectra(Kiss.Spectra spectra)
	{
		for(int i = 0; i < spectra.Real.Length; ++i)
			Console.WriteLine("{0:F}+{1:F}i", spectra.Real[i], spectra.Imaginary[i]);
	}

	private static void PrintOutputMask(Kiss.Network network)
	{
		var outputMask = network.OutputMask;
		var labels = network.OutputLabels;
		for(int i = 0; i < outputMask.Length; ++i)
			Console.WriteLine("{0:S}: {1:B}", labels[i], outputMask[i]);
	}

	public static void Result(float[] result, Kiss.Network network)
	{
		Console.WriteLine("Thread id: {0:D}", System.Environment.CurrentManagedThreadId);

		Console.WriteLine("Got result");
		string[] outputLabels = network.OutputLabels;
		for(int i = 0; i < result.Length; ++i)
			Console.WriteLine("{0:S}: {1:F}", outputLabels[i], result[i]);
	}

	public static int Main(string[] args)
	{
		Kiss.VersionFixed version = Kiss.VersionFixed.GetVersion();
		Console.WriteLine("libkissinference version: {0:D}.{1:D}.{2:D}", version.Major, version.Minor, version.Patch);
		Console.WriteLine("Thread id: {0:D}", System.Environment.CurrentManagedThreadId);

		Console.WriteLine("Eis: {0:S}", "r-rc(r-w)");
		Console.WriteLine("Relaxis: {0:S}", Kiss.Translators.EisToRelaxIs("r-rc(r-w)"));
		Console.WriteLine("CDC: {0:S}", Kiss.Translators.EisToCdc("r-rc(r-w)"));

		var spectra = new Kiss.Spectra(Kiss.Utils.CreateRange(1, 10, 10, true), Kiss.Utils.CreateRange(1, 10, 10, true), Kiss.Utils.CreateRange(1, 10, 10, true));

		Console.WriteLine("A spectra:");
		PrintSpectra(spectra);

		spectra.Filter(5);
		Console.WriteLine("Filtered:");
		PrintSpectra(spectra);

		if(args.Length < 1)
		{
			Console.WriteLine("Unable to test network inference as no network file name was provided");
			return 1;
		}

		var filename = args[0];
		Console.WriteLine("Loading: {0:S}", filename);

		try
		{
			var net = new Kiss.Network(filename, false);
			Console.WriteLine("Input size: {0:D} OutputSize: {1:D} Purpose: {2:S} InputLabel: {3:S}", net.InputSize, net.OutputSize, net.Purpose, net.InputLabel);
			spectra.Resample(net.InputSize/2);
			Console.WriteLine("Expanded spectra:");
			PrintSpectra(spectra);
			var signal = net.Run(spectra, Result);
			signal.WaitOne();
			Console.WriteLine("Awaited\n");

			PrintOutputMask(net);
			var newMask = Enumerable.Repeat(true, net.OutputSize).ToArray();
			newMask[7] = false;
			net.OutputMask = newMask;
			Console.WriteLine("");

			signal = net.Run(spectra, Result);
			signal.WaitOne();
			Console.WriteLine("Awaited\n");
		}
		catch(System.IO.FileLoadException e)
		{
			Console.WriteLine(e.Message);
		}

		return 0;
	}
}
