all: kissinference kissinference_test

kissinference:
	dotnet msbuild kissinferencesharp.csproj -t:Build

kissinference_test: kissinference
	dotnet msbuild kissinference_test.csproj -t:Build

clean:
	dotnet msbuild kissinferencesharp.csproj -t:Clean
	dotnet msbuild kissinference_test.csproj -t:Clean
	rm -r doc/html doc/latex || true

.PHONY: doc
doc:
	doxygen doc/libkissinference.doxygen

restore:
	dotnet restore kissinferencesharp.sln
	dotnet restore kissinference_test.csproj

run:
	dotnet bin/net8/kissinference.dll ../scripttraining/simplenet100-6.onnx
